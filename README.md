
Bio3D Normal Mode Analysis
==========================

Bio3D-NMA is an [R](http://www.r-project.org/) package that facilitates normal mode analysis (NMA) calculation of protein structure(s).

It is a package of the larger [Bio3D](https://bitbucket.org/Grantlab/bio3d) family containing utilities for the analysis of protein structure, sequence and trajectory data. It is currently distributed as platform independent source code under the [GPL version 2 license](http://www.gnu.org/copyleft/gpl.html).

Features
--------

-   Normal modes calculation on either a single protein structure or an ensemble of aligned protein structures.
-   Multiple elastic network models for efficient analysis of protein structure flexibility including the popular anisotropic network model (ANM), the associated parameter-free ANM, and the more sophisticated C-alpha force field derived from fitting to the Amber94 all-atom potential.
-   Ensemble normal mode analysis on large structure sets to explore evolutionary dynamics and structure dependent protein flexibility.
-   All-atom elastic network models with rotation-translation block (RTB) based approximation.
-   Calculate flexibility profiles, perform cross-correlation analysis, modes visualization in PyMOL, and more.

Installation
------------

The development version of Bio3d-nma is available from BitBucket:

``` r
install.packages("devtools")
devtools::install_bitbucket("Grantlab/bio3d-nma")
```

Make sure you have installed [Bio3D](https://bitbucket.org/Grantlab/bio3d) core package prior to installing Bio3D-nma:

``` r
devtools::install_bitbucket("Grantlab/bio3d/bio3d-core", ref="core")
```

Basic usage for single structre analysis
----------------------------------------

``` r
# load library
library(bio3d.nma)

# read pdb
pdb <- read.pdb("1hel")
#>   Note: Accessing on-line PDB file

# calculate modes
modes <- nma(pdb)
#>  Building Hessian...     Done in 0.012 seconds.
#>  Diagonalizing Hessian...    Done in 0.074 seconds.

# plot fluctuations
plot(modes, sse=pdb)
```

![](man/figures/README-syntax_demo-1.png)

Basic usage for ensemble analysis
---------------------------------

``` r
# fetch structures
ids <- c("1rx8_A", "1rd7_B", "4fhb_A", "1rx4_A")
files <- get.pdb(ids, split = TRUE, path = tempdir())
#> 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |==================                                                    |  25%
  |                                                                            
  |===================================                                   |  50%
  |                                                                            
  |====================================================                  |  75%
  |                                                                            
  |======================================================================| 100%

## Sequence Alignement
pdbs <- pdbaln(files, outfile = tempfile())
#> Reading PDB files:
#> /tmp/Rtmps8t5XA/split_chain/1rx8_A.pdb
#> /tmp/Rtmps8t5XA/split_chain/1rd7_B.pdb
#> /tmp/Rtmps8t5XA/split_chain/4fhb_A.pdb
#> /tmp/Rtmps8t5XA/split_chain/1rx4_A.pdb
#> ....
#> 
#> Extracting sequences
#> 
#> pdb/seq: 1   name: /tmp/Rtmps8t5XA/split_chain/1rx8_A.pdb 
#> pdb/seq: 2   name: /tmp/Rtmps8t5XA/split_chain/1rd7_B.pdb 
#> pdb/seq: 3   name: /tmp/Rtmps8t5XA/split_chain/4fhb_A.pdb 
#> pdb/seq: 4   name: /tmp/Rtmps8t5XA/split_chain/1rx4_A.pdb

## Normal mode analysis on aligned data
modes <- nma(pdbs)
#> 
#> Details of Scheduled Calculation:
#>   ... 4 input structures 
#>   ... storing 471 eigenvectors for each structure 
#>   ... dimension of x$U.subspace: ( 477x471x4 )
#>   ... coordinate superposition prior to NM calculation 
#>   ... aligned eigenvectors (gap containing positions removed)  
#>   ... estimated memory usage of final 'eNMA' object: 6.9 Mb 
#> 
#> 
  |                                                                            
  |                                                                      |   0%
  |                                                                            
  |==================                                                    |  25%
  |                                                                            
  |===================================                                   |  50%
  |                                                                            
  |====================================================                  |  75%
  |                                                                            
  |======================================================================| 100%

## Plot fluctuation data
plot(modes, pdbs=pdbs)
```

![](man/figures/README-syntax_demo2-1.png)

Citing Bio3D and Bio3D NMA
--------------------------

-   The Bio3D packages for structural bioinformatics <br> Grant, Skjærven, Yao, (2020) *Protein Science* <br> ( [Abstract](https://onlinelibrary.wiley.com/doi/abs/10.1002/pro.3923) | [PubMed]() | [PDF]() )

-   Integrating protein structural dynamics and evolutionary analysis with Bio3D. <br> Skjærven, Yao, Scarabelli, Grant, (2014) *BMC Bioinformatics* **15**, 399 <br> ( [Abstract](http://www.biomedcentral.com/1471-2105/15/399/abstract) | [PubMed](http://www.ncbi.nlm.nih.gov/pubmed/25491031) | [PDF](http://www.biomedcentral.com/content/pdf/s12859-014-0399-6.pdf) )

-   Bio3D: An R package for the comparative analysis of protein structures. <br> Grant, Rodrigues, ElSawy, McCammon, Caves, (2006) *Bioinformatics* **22**, 2695-2696 <br> ( [Abstract](http://bioinformatics.oxfordjournals.org/cgi/content/abstract/22/21/2695) | [PubMed](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=retrieve&db=pubmed&list_uids=16940322&dopt=Abstract) | [PDF](http://bioinformatics.oxfordjournals.org/content/22/21/2695.full.pdf) )
