Package: bio3d.nma
Title: Bio3D Normal Mode Analysis 
Version: 1.0-0.9000
Authors@R: c(person("Barry", "Grant", role=c("aut", "cre"),
                     email="larsss@gmail.com"),
             person("Xin-Qiu", "Yao", role="aut"),
             person("Lars", "Skjaerven", role="aut"))
Author: Barry Grant [aut, cre],
  Xin-Qiu Yao [aut],
  Lars Skjaerven [aut]
Description: Normal mode analysis (NMA) module of Bio3D. NMA of protein
  structures is a computational approach for studying and
  characterizing protein flexibility. Current functionality entails normal
  modes calculation on either a single protein structure or an ensemble
  of aligned protein structures. 
Maintainer: Barry Grant <larsss@gmail.com>
VignetteBuilder: knitr
Depends:
   R (>= 3.3.0),
   bio3d.core
Imports:
    parallel,
    graphics,
    grDevices,
    stats,
    utils,
    lattice
Suggests:
    knitr,
    bigmemory,
    testthat (>= 0.9.1)
License: GPL (>= 2)
URL: http://thegrantlab.org/bio3d/, http://bitbucket.org/Grantlab/bio3d
Encoding: UTF-8
LazyData: true
RoxygenNote: 7.1.2
